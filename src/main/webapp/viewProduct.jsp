<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 06.04.2019
  Time: 11:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Product</title>
</head>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<ul class="list-group">
    <c:choose>
        <c:when test="${product.isPresent()}">
            <c:set var="product" value="${product.get()}"/>
            <table class="table table-dark table-bordered" align="center" style="width: 80ex">
                <thead>
                <tr style="background-color: darkgray">
                    <th scope="col">Product's name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Categories</th>
                    <th scope="col">Price</th>
                    <th scope="col">Photo</th>
                    <th scope="col">Availability</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row"><p><a
                            href="${pageContext.request.contextPath}/viewProduct?id=${product.getId()}">${product.getName()}</a>
                    </p></th>
                    <td><p>${product.getDescription()}</p></td>
                    <td><p>${product.getCategories()}</p></td>
                    <td><p>${product.getPrice()}</p></td>
                    <td><img width="50px" height="50px" src="${product.getImage()}"/></td>
                    <td><p>${product.getAvailability()}</p></td>
                </tr>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            There's no product!
        </c:otherwise>
    </c:choose>
</ul>
<jsp:include page="footer.jsp"/>
</body>
</html>
