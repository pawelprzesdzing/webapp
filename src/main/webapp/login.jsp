<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 06.04.2019
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>Login</title>
</head>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<div align="center">
    <form action="${pageContext.request.contextPath}/login" method="post">
        <h1 class="display-5">Sign in</h1>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter login" type="text" name="login"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter password" type="password"
                   name="password"/>
        </div>
        <p>
            <input class="btn btn-primary" type="submit" name="submit" value="Sign in"/>
        </p>
    </form>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
