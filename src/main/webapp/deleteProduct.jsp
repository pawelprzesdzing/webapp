<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 06.04.2019
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete product</title>
</head>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<div align="center">
    <form action="${pageContext.request.contextPath}/deleteProduct" method="post">
        <h1 class="display-5" style="background-color: antiquewhite">Delete product by id</h1>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter product's id" type="text" name="id"/>
        </div>
        <p>
            <input class="btn btn-primary" type="submit" name="submit" value="Delete product"/>
        </p>
    </form>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
