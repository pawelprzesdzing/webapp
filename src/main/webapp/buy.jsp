<%@ page import="com.sda.przesdzing.servlet.Domain.Product" %>
<%@ page import="com.sda.przesdzing.servlet.Order.Order" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 14.04.2019
  Time: 09:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<head>
    <title>Buy</title>
</head>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<div>
    <%
        int i = 1;
        List<Order> orderList = (List<Order>) request.getAttribute("orderList");
        if (orderList != null) {
            for (Order order : orderList) {
                for (Map.Entry<Product, Integer> entry : order.getCart().get().entrySet()) {
    %>
    <div align="center" style="color: white; background-color: dimgrey">
        <div><b><i><%="Products name: " + entry.getKey().getName()%>
        </i>
        </b>
        </div>
        <div><%="Price: " + entry.getKey().getPrice() + " $"%>
        </div>
        <div><%="Quantity: " + entry.getValue() + " pcs."%>
        </div>
        <div><%="Total price for this item: " + entry.getKey().getPrice() * entry.getValue() + " $"%>
        </div>
        <%
            } %>
        <br>
        <div><u><%="User's login: " + orderList.iterator().next().getUser().getLogin()%>
        </u>
        </div>
        <div><%="Name and surname: " + order.getBuyerName() + " " + order.getBuyerSurname()%>
        </div>
        <div>
            <%="Address: " + order.getBuyerStreet() + " " + order.getBuyerHomeNumber() + " "
                    + order.getBuyerZipCode() + " " + order.getBuyerCity()%>
        </div>
        <div><h2><%="Total price: " + order.getOrderTotalPrice() + " $"%>
        </h2>
        </div>
        <div style="background-color: darkgrey"><h2><i><b>
                <%="Order number: " + i++%>
        </i></b>
        </h2>
        </div>
        <style>
            .dotted {
                color: #fff;
                background-color: #fff;
            }
        </style>
        <hr class='dotted'/>
        <%
            }%>
    </div>
    <%
        }
    %>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
