<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 06.04.2019
  Time: 10:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Databases products</title>
</head>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
    <table class="table table-dark table-bordered" align="center" style="width: 70%; height: 15em">
        <thead>
        <tr style="background-color: darkgray; width: 20px">
            <th scope="col">Product's name</th>
            <th scope="col">Description</th>
            <th scope="col">Categories</th>
            <th scope="col">Price</th>
            <th scope="col">Photo</th>
            <th scope="col">Availability</th>
            <th scope="col">Buy it!</th>
        </tr>
        </thead>
        <c:forEach items="${dbproducts}" var="product">
        <tbody>
        <tr>
            <th scope="row"><p><a
                    href="${pageContext.request.contextPath}/viewDBProduct?id=${product.getId()}">${product.getName()}</a>
            </p></th>
            <td><p>${product.getDescription()}</p></td>
            <td><p>${product.getCategories()}</p></td>
            <td><p>${product.getPrice()}</p></td>
            <td><img width="50px" height="50px" src="${product.getImage()}"/></td>
            <td><p>${product.getAvailability()}</p></td>
            <td>
                <div>
                    <form action="${pageContext.request.contextPath}/addToCart" method="post">
                        <input type="hidden" name="productId" value="${product.getId()}"/>
                        <input name="quantity" align="center" size="2" maxlength="2" style="width:3ex" type="text"
                               value="1">
                        <button type="submit" class="btn btn-danger">Buy!
                        </button>
                    </form>
                </div>
                <div>
                </div>
            </td>
        </tr>
        </tbody>
        </c:forEach>
    </table>
<jsp:include page="footer.jsp"/>
</body>
</html>
