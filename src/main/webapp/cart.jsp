<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.sda.przesdzing.servlet.Cart.Cart" %>
<%@ page import="com.sda.przesdzing.servlet.Domain.Product" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 13.04.2019
  Time: 10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<head>
    <title>Cart</title>
</head>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<%
    Cart cart = (Cart) session.getAttribute("cart");
    long totalPrice = 0;
    if (cart != null) {
        for (Map.Entry<Product, Integer> entry : cart.get().entrySet()) {
            totalPrice = totalPrice + entry.getKey().getPrice() * entry.getValue();
%>
<p>
    <table class="table table-dark table-bordered" align="center" style="width: 80ex">
        <thead>
        <tr style="background-color: darkgray" align="center">
            <th scope="col">Product's name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price €</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
<p><%=entry.getKey().getName()%>
</p></td>
<td>
    <div align="center">
        <form action="${pageContext.request.contextPath}/addToCart" method="post">
            <input type="hidden" name="productId" value="<%= entry.getKey().getId()%>">
            <input type="hidden" name="quantity" value="1">
            <button type="submit" class="btn btn-success">
                <i class="fas fa-plus"></i>
            </button>
        </form>
    </div>
    <div align="center">
        <%=entry.getValue()%>
    </div>
    <br>
    <div align="center">
        <form action="${pageContext.request.contextPath}/addToCart" method="post">
            <input type="hidden" name="productId" value="<%= entry.getKey().getId()%>">
            <input type="hidden" name="quantity" value="-1">
            <button type="submit" class="btn btn-success">
                <i class="fas fa-minus"></i>
            </button>
        </form>
    </div>
</td>
<td align="center">
    <form>
        <%=entry.getKey().getPrice() * entry.getValue()%>
    </form>
</td>
</tr>
</tbody>
</table>
</p>
<%
    }
%>
<td>
    <div style="background-color: white; color: dimgrey" align="center">
        <h2>Total price:
            <%=totalPrice%> €
        </h2>
    </div>
    <div align="center">
        <a href="${pageContext.request.contextPath}/buy-data" methods="post">
            <button type="button" class="btn btn-danger">
                Go to the order!
            </button>
        </a>
    </div>
</td>
<div>
    <%
    } else {
    %>
    <%--<h2 style="color: white" align="center"><i>There's no product in cart!</i></h2>--%>
    <div align="center">
        <img src="https://iticsystem.com/img/empty-cart.png">
    </div>
    <div align="center">
        <a href="allProducts">Click here to continue shopping</a>
    </div>
    <%
        }
    %>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
