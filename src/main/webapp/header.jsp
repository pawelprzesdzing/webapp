<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.sda.przesdzing.servlet.Domain.User" %>
<%@ page import="com.sda.przesdzing.servlet.User.Roles" %>
<%@ page import="java.time.Instant" %>
<%@ page import="java.util.Date" %>
<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 30.03.2019
  Time: 12:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" data-auto-replace-svg="nest"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">
<script src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" data-auto-replace-svg="nest"></script>
<html>
<head>
    <title>Header</title>
</head>
<body>
<div>
    <br>
    <div style="margin-left: 30px">
        <h1 class="display-3">MENU</h1>
    </div>
    <div style="float: right; margin-top: -89px; margin-right: 35px">
        <div>
            <%
                User loggedUser = (User) session.getAttribute("loggedUser");
                if (loggedUser == null) {
            %>
            <a href="${pageContext.request.contextPath}/login">
                <button style="display: inline-block" type="button" class="btn btn-info">Sign in</button>
            </a>
            <a href="${pageContext.request.contextPath}/register">
                <button type="button" class="btn btn-info">Register</button>
            </a>
            <%
            } else {
            %>
            <a style="margin-right: 10px; font-style: italic; font-size: 20px">
                <%
                    out.println(String.format("Hello %s", loggedUser.getLogin() + "!"));
                %>
            </a>

            <a href="${pageContext.request.contextPath}/buy">
                <button type="button" class="btn btn-info">Orders history</button>
            </a>
            <a href="${pageContext.request.contextPath}/logout" methods="get">
                <button type="button" class="btn btn-info">Logout</button>
            </a>
            <%
                }
            %>
        </div>
        <div style="margin-top: 10px; margin-left: 81px">
            <a href="${pageContext.request.contextPath}/addToCart" methods="get">
                <button style="display: inline-block; float: right" type="button" class="btn btn-danger"><i
                        style="font-size: 48px"
                        class="fas fa-shopping-cart"></i>
                </button>
            </a>
        </div>
    </div>
    <div>
        <p style="margin-left: 24px">
            <a href="${pageContext.request.contextPath}/home">
                <button type="button" class="btn btn-primary">Home</button>
            </a>
            <a href="${pageContext.request.contextPath}/allProducts">
                <button type="button" class="btn btn-success">No databases products</button>
            </a>
            <a href="${pageContext.request.contextPath}/dbproducts">
                <button type="button" class="btn btn-warning">Databases products</button>
            </a>
            <%
                if (loggedUser != null && loggedUser.getRoles().contains(Roles.ADMIN)) {
            %>
            <a href="${pageContext.request.contextPath}/addProduct">
                <button type="button" class="btn btn-success">Add Product</button>
            </a>
            <a href="${pageContext.request.contextPath}/deleteProduct">
                <button type="button" class="btn btn-success">Delete product</button>
            </a>
            <%
                }
            %>
        </p>
    </div>
    <div align="center" style="float:bottom; line-height: 30px">
        <br>
        <div align="center">
            <p>Current date and time: <%=Date.from(Instant.now())%>
            </p>
        </div>
    </div>
</div>
</body>
</html>
