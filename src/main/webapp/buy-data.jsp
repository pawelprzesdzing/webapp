<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 13.04.2019
  Time: 14:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<head>
    <title>Buy-data</title>
</head>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<div align="center">
    <form action="${pageContext.request.contextPath}/buy-data" method="post">
        <h1 class="display-5">Address data</h1>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter name" type="text" name="buyerName"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter surname" type="text"
                   name="buyerSurname"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter street" type="text" name="buyerStreet"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter home number" type="text"
                   name="buyerHomeNumber"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter zip code" type="text"
                   name="buyerZipCode"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter city" type="text"
                   name="buyerCity"/>
        </div>
        <div>
            <input class="btn btn-primary" type="submit" name="submit" value="Go to the payment"/>
        </div>
    </form>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
