<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">
<html>
<body>
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<div align="center">
    <form action="${pageContext.request.contextPath}/home" method="get">
        <div style="background-color: antiquewhite">
            <h1 class="display-5">Search</h1>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" type="text" name="category"
                   placeholder="Enter category"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" type="text" name="name"
                   placeholder="Enter product's name"/>
        </div>
        <p>
            <input class="btn btn-primary" type="submit" name="submit" value="Submit"/>
        </p>
    </form>
</div>
<ul class="list-group">
    <c:forEach items="${products}" var="product">
        <table class="table table-dark table-bordered" align="center" style="width: 70%; height: 15em">
            <thead>
            <tr style="background-color: darkgray; width: 20px">
                <th scope="col">Product's name</th>
                <th scope="col">Description</th>
                <th scope="col">Categories</th>
                <th scope="col">Price</th>
                <th scope="col">Photo</th>
                <th scope="col">Buy it!</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"><p><a
                        href="${pageContext.request.contextPath}/viewProduct?id=${product.getId()}">${product.getName()}</a>
                </p></th>
                <td><p>${product.getDescription()}</p></td>
                <td><p>${product.getCategories()}</p></td>
                <td><p>${product.getPrice()}</p></td>
                <td><img width="50px" height="50px" src="${product.getImage()}"/></td>
                <td>
                    <div>
                        <form action="${pageContext.request.contextPath}/addToCart" method="post">
                            <input type="hidden" name="productId" value="${product.getId()}"/>
                            <input name="quantity" align="center" size="2" maxlength="2" style="width:3ex" type="text"
                                   value="1">
                            <button type="submit" class="btn btn-danger">Buy!
                            </button>
                        </form>
                    </div>
                    <div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </c:forEach>
</ul>
<div align="right">
    <h2 class="whiteColour" align="center">Advertisement!</h2>
</div>
<div align="center">
    <img style="box-shadow: 3px 3px 10px white, darkred -3px 3px 10px, 3px -3px 10px" width="300px" height="280px"
         src="${advertisement}">
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
