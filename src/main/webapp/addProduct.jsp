<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 06.04.2019
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Products</title>
</head>
<body>
<link rel="stylesheet" href="style.css">
<style>
    body {
        background-image: url("https://images.unsplash.com/photo-1534448177492-6d698f12a59a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
        background-size: cover;
    }
</style>
<jsp:include page="header.jsp"/>
<div align="center">
    <form action="${pageContext.request.contextPath}/addProduct" method="post">
        <h1 class="display-5" style="background-color: antiquewhite">New product</h1>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter product's name" type="text" name="name"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter description" type="text"
                   name="description"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter category" type="text" name="categories"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter price" type="text" name="price"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter availability" type="text"
                   name="availability"/>
        </div>
        <div class="form-group">
            <input class="form-control" style="width:30ex" placeholder="Enter photo's URL" type="text" name="image"/>
        </div>
        <div>
            <input class="btn btn-primary" type="submit" name="submit" value="Add product"/>
        </div>
    </form>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
