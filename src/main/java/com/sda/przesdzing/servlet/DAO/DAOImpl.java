package com.sda.przesdzing.servlet.DAO;

import com.sda.przesdzing.servlet.DB.Connection;
import com.sda.przesdzing.servlet.Domain.ProductV2;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class DAOImpl implements DAO {
    public Connection connection;
    private ArrayList<ProductV2> products;
    private ProductV2 product;

    @Override
    public ArrayList<ProductV2> getAllProducts() throws SQLException {
        connection = new Connection();
        Statement statement = null;
        try {
            statement = connection.getStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String sqlStr = "select * from products";
        ResultSet rs = statement.executeQuery(sqlStr);
        products = connection.getProductsList();

        while (rs.next()) {
            int id = rs.getInt("id");
            String name1 = rs.getString("name");
            String description = rs.getString("description");
            long price = rs.getLong("price");

            ProductV2 product = new ProductV2(id, name1, description, Arrays.asList("test"), price, null, 2);
            products.add(product);
        }
        return products;
    }

    @Override
    public Optional<ProductV2> getProductById(int id) throws SQLException {
        connection = new Connection();
        Statement statement = null;
        try {
            statement = connection.getStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String sqlStr = "select * from products where products.id = " + id;
        ResultSet rs = statement.executeQuery(sqlStr);
        //products = connection.getProductsList();

        while (rs.next()) {
            String name1 = rs.getString("name");
            String description = rs.getString("description");
            long price = rs.getLong("price");

            product = new ProductV2(id, name1, description, Arrays.asList("test"), price, null, 2);
        }
        return Optional.of(product);
    }
}
