package com.sda.przesdzing.servlet.DAO;

import com.sda.przesdzing.servlet.Domain.ProductV2;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public interface DAO {
    ArrayList<ProductV2> getAllProducts() throws SQLException;
    Optional<ProductV2> getProductById(int id) throws SQLException;
}
