package com.sda.przesdzing.servlet.DAO;

import com.sda.przesdzing.servlet.Order.Order;

import java.util.ArrayList;
import java.util.List;

public class DAOOrderDb {
    private static DAOOrderDb instance;
    private List<Order> orderList;

    private DAOOrderDb() {
        orderList = new ArrayList<>();
    }

    public static DAOOrderDb getInstance() {
        if (instance == null) {
            instance = new DAOOrderDb();
        }
        return instance;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

}
