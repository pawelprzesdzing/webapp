package com.sda.przesdzing.servlet.DAO;

import com.sda.przesdzing.servlet.Domain.User;
import com.sda.przesdzing.servlet.User.Roles;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.*;

public class DAOUserDb {
    private static DAOUserDb instance;
    private List<User> userList;

    private DAOUserDb() {
        userList = new ArrayList<>();
        String adminPassword = "admin";
        String ppPassword = "pp";
        String adminSha256hex = DigestUtils.sha256Hex(adminPassword);
        String ppsSha256hex = DigestUtils.sha256Hex(ppPassword);
        userList.add(new User("admin", adminSha256hex, "admin@admin.com",
                Arrays.asList(Roles.ADMIN, Roles.BASIC_USER)));
        userList.add(new User("pp", ppsSha256hex, "pp@pp.com",
                Collections.singletonList(Roles.BASIC_USER)));
    }

    public static DAOUserDb getInstance() {
        if (instance == null) {
            instance = new DAOUserDb();
        }
        return instance;
    }

    public Optional<User> findByEmail(String email) {
        for (User user : userList) {
            if (user.getEmail().equals(email)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    public Optional<User> findByLogin(String login) {
        for (User user : userList) {
            if (user.getLogin().equals(login)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    public Optional<User> findByLoginAndPassword(String login, String password) {
        return userList.stream()
                .filter(x -> x.getLogin().equalsIgnoreCase(login))
                .filter(x -> x.getPassword().equals(password))
                .findFirst();
    }

    public List<User> getUserList() {
        return userList;
    }

    public Optional<User> getUserByLogin(String login) {
        return userList.stream()
                .filter(x -> x.getLogin().equals(login))
                .findFirst();
    }

    public User createNewUser(String login, String password, String email, List<Roles> roles) {
        String sha256hex = DigestUtils.sha256Hex(password);
        User user = new User(login, sha256hex, email, Collections.singletonList(Roles.BASIC_USER));
        userList.add(user);
        return user;
    }

}
