package com.sda.przesdzing.servlet.DAO;

import com.sda.przesdzing.servlet.Domain.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class DAOProductDb {
    private static DAOProductDb instance;
    private List<Product> productList;

    private DAOProductDb() {
        productList = new ArrayList<>();
        productList.add(new Product(0L, "Hammer", "Its a spiking tool",
                Arrays.asList("Heavy tools", "Big tools"), 12L, "https://www.dekarze.pl/environment/cache/images/500_500_productGfx_23ec79ce740f2862cfd473dd24901716.jpg", 1000));
        productList.add(new Product(1L, "Spike", "You can use this at home",
                Arrays.asList("Light tools", "Metal department"), 2L, "https://sklep.geoproduct.pl/environment/cache/images/500_500_productGfx_8c769fefc0f6a74bf86d62a3aea6a04f.jpg", 1000));
        productList.add(new Product(2L, "Screwdriver", "Basic tool to screw",
                Arrays.asList("Light tools", "Builder department"), 4L, "https://wior.pl/19822-large_default/srubokret-bit-krzyzakowy-pz-wielkosc-2-dlugosc-calkowita-z-lbem-210-mm-pomaranczowe.jpg", 1000));
        productList.add(new Product(3L, "Ladder", "Basic tool to painting Your ceiling!",
                Arrays.asList("Light tools", "Builder department"), 40L, "https://images.obi.pl/product/PL/1500x1500/553866_1.jpg", 1000));
        productList.add(new Product(4L, "Protective gloves", "Protect Your hands",
                Arrays.asList("Light tools", "Protective materials department"), 6L, "https://sklep.lahtipro.pl/1124-large_default/rekawice-ochronne-bhp-ocieplane-powlekane-lateksem-lahti-pro-l2508.jpg", 1000));
        productList.add(new Product(5L, "Googles", "Protect Your eyes",
                Arrays.asList("Light tools", "Protective materials department"), 8L, "https://images.obi.pl/product/PL/1500x1500/431357_1.jpg", 1000));
        productList.add(new Product(6L, "Drill", "Tool for making holes",
                Arrays.asList("Light tools", "Builder department"), 12L, "https://images.obi.pl/product/PL/1500x1500/262109_2.jpg", 1000));
        productList.add(new Product(7L, "Brush", "Painting tool",
                Arrays.asList("Light tools", "Painting department"), 9L, "https://static01.leroymerlin.pl/files/media/image/288/1307288/pedzel%20do%20farb%20rozpuszczalnikowych.jpg", 1000));
        productList.add(new Product(8L, "Roller", "Painting tool",
                Arrays.asList("Light tools", "Painting department"), 15L, "https://static3.redcart.pl/templates/images/thumb/10058/1500/1500/pl/0/templates/images/products/10058/177e7f7775e9c4540d134260df19206f.jpg", 1000));
        productList.add(new Product(9L, "Painting tape", "Painting tool",
                Arrays.asList("Light tools", "Painting department"), 6L, "https://images.obi.pl/product/PL/1500x1500/268214_1.jpg", 1000));

    }

    public static DAOProductDb getInstance() {
        if (instance == null) {
            instance = new DAOProductDb();
        }
        return instance;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public Optional<Product> getProductById(int id) {
        return productList.stream()
                .filter(x -> x.getId() == id)
                .findFirst();
    }

    public long createNewProduct(String name, String description, List<String> categories, long price, String image, int availability) {
        int idCounter = getProductList().size();
        Product product = new Product(idCounter, name, description, categories, price, image, availability);
        productList.add(product);
        return product.getId();
    }

    public void deleteProductById(int id) {
        productList.remove(id);
    }
}
