package com.sda.przesdzing.servlet.DB;

import com.sda.przesdzing.servlet.Domain.ProductV2;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Connection {
    private ArrayList<ProductV2> products;

    public Statement getStatement() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        java.sql.Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/products?serverTimezone=UTC", "root", "PPrzesdzing");
        return connection.createStatement();
    }

    public  ArrayList<ProductV2> getProductsList(){
        if (products == null) {
            products = new ArrayList<>();
        }
        return products;
    }
}
