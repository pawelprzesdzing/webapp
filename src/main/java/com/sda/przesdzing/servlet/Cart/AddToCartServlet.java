package com.sda.przesdzing.servlet.Cart;

import com.sda.przesdzing.servlet.DAO.DAOProductDb;
import com.sda.przesdzing.servlet.Domain.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/addToCart")
public class AddToCartServlet extends HttpServlet {
    private DAOProductDb db;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest.getRequestDispatcher("/cart.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        int productId = Integer.parseInt(httpServletRequest.getParameter("productId"));
        Integer quantity = Integer.valueOf(httpServletRequest.getParameter("quantity"));

        Optional<Product> productById = db.getProductById(productId);
        Cart cart = (Cart) httpServletRequest.getSession().getAttribute("cart");

        if (productById.isPresent()) {
            if (cart == null) {
                cart = new Cart();
            }
            cart.updateQuantity(productById.get(), quantity);
            httpServletRequest.getSession().setAttribute("cart", cart);
        }

        httpServletRequest.getRequestDispatcher("/cart.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    public void init() throws ServletException {
        db = DAOProductDb.getInstance();
    }
}
