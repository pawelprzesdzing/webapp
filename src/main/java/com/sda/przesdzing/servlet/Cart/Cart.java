package com.sda.przesdzing.servlet.Cart;

import com.sda.przesdzing.servlet.Domain.Product;

import java.util.HashMap;
import java.util.Map;

public class Cart {
    private Map<Product, Integer> products;

    public Cart() {
        products = new HashMap<>();
    }

    public Map<Product, Integer> get() {
        return products;
    }

    public void add(Product product, Integer quantity) {
        if (products.containsKey(product)) {
            Integer oldQuantity = products.get(product);
            products.put(product, oldQuantity + quantity);
        } else {
            products.put(product, quantity);
        }
    }

    public void updateQuantity(Product product, Integer quantity) {
        if (products.containsKey(product)) {
            Integer oldQuantity = products.get(product);
            if (oldQuantity + quantity > 0) {
                products.put(product, oldQuantity + quantity);
            } else {
                products.remove(product);
            }
        } else if (quantity > 0) {
            products.put(product, quantity);
        }
    }

    public long getTotalPrice() {
        long totalPrice = 0;

        for (Map.Entry<Product, Integer> entry : this.get().entrySet()) {
            totalPrice = totalPrice + entry.getKey().getPrice() * entry.getValue();
        }
        return totalPrice;
    }
}
