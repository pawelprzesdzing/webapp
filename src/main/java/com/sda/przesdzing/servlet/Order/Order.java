package com.sda.przesdzing.servlet.Order;

import com.sda.przesdzing.servlet.Cart.Cart;
import com.sda.przesdzing.servlet.Domain.User;

import java.util.Objects;

public class Order {
    private User user;
    private Cart cart;
    private Long orderTotalPrice = 0L;
    private String buyerName;
    private String buyerSurname;
    private String buyerStreet;
    private String buyerHomeNumber;
    private String buyerZipCode;
    private String buyerCity;

    public Order(User user, Cart cart, Long orderTotalPrice,
                 String buyerName, String buyerSurname, String buyerStreet,
                 String buyerHomeNumber, String buyerZipCode, String buyerCity) {
        this.user = user;
        this.cart = cart;
        this.orderTotalPrice = orderTotalPrice;
        this.buyerName = buyerName;
        this.buyerSurname = buyerSurname;
        this.buyerStreet = buyerStreet;
        this.buyerHomeNumber = buyerHomeNumber;
        this.buyerZipCode = buyerZipCode;
        this.buyerCity = buyerCity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Long getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(Long orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerSurname() {
        return buyerSurname;
    }

    public void setBuyerSurname(String buyerSurname) {
        this.buyerSurname = buyerSurname;
    }

    public String getBuyerStreet() {
        return buyerStreet;
    }

    public void setBuyerStreet(String buyerStreet) {
        this.buyerStreet = buyerStreet;
    }

    public String getBuyerHomeNumber() {
        return buyerHomeNumber;
    }

    public void setBuyerHomeNumber(String buyerHomeNumber) {
        this.buyerHomeNumber = buyerHomeNumber;
    }

    public String getBuyerZipCode() {
        return buyerZipCode;
    }

    public void setBuyerZipCode(String buyerZipCode) {
        this.buyerZipCode = buyerZipCode;
    }

    public String getBuyerCity() {
        return buyerCity;
    }

    public void setBuyerCity(String buyerCity) {
        this.buyerCity = buyerCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(user, order.user) &&
                Objects.equals(cart, order.cart) &&
                Objects.equals(orderTotalPrice, order.orderTotalPrice) &&
                Objects.equals(buyerName, order.buyerName) &&
                Objects.equals(buyerSurname, order.buyerSurname) &&
                Objects.equals(buyerStreet, order.buyerStreet) &&
                Objects.equals(buyerHomeNumber, order.buyerHomeNumber) &&
                Objects.equals(buyerZipCode, order.buyerZipCode) &&
                Objects.equals(buyerCity, order.buyerCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, cart, orderTotalPrice, buyerName, buyerSurname, buyerStreet, buyerHomeNumber, buyerZipCode, buyerCity);
    }
}
