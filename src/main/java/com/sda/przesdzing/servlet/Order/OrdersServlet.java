package com.sda.przesdzing.servlet.Order;

import com.sda.przesdzing.servlet.DAO.DAOOrderDb;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/buy")
public class OrdersServlet extends HttpServlet {
    private DAOOrderDb db;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        List<Order> orderList = db.getOrderList();
        httpServletRequest.setAttribute("orderList", orderList);
        httpServletRequest.getSession().removeAttribute("cart");

        httpServletRequest
                .getRequestDispatcher("/buy.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    public void init() throws ServletException {
        db = DAOOrderDb.getInstance();
    }
}
