package com.sda.przesdzing.servlet.Order;

import com.sda.przesdzing.servlet.Cart.Cart;
import com.sda.przesdzing.servlet.DAO.DAOOrderDb;
import com.sda.przesdzing.servlet.Domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/buy-data")
public class OrderBuyDataServlet extends HttpServlet {
    private DAOOrderDb db;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest.getRequestDispatcher("/buy-data.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        User loggedUser = (User) httpServletRequest.getSession().getAttribute("loggedUser");
        Cart cart = (Cart) httpServletRequest.getSession().getAttribute("cart");
        String buyerName = httpServletRequest.getParameter("buyerName");
        String buyerSurname = httpServletRequest.getParameter("buyerSurname");
        String buyerStreet = httpServletRequest.getParameter("buyerStreet");
        String buyerHomeNumber = httpServletRequest.getParameter("buyerHomeNumber");
        String buyerZipCode = httpServletRequest.getParameter("buyerZipCode");
        String buyerCity = httpServletRequest.getParameter("buyerCity");
        long totalPrice = cart.getTotalPrice();

        db.getOrderList().add(new Order(loggedUser, cart, totalPrice, buyerName, buyerSurname, buyerStreet, buyerHomeNumber,
                buyerZipCode, buyerCity));

        httpServletResponse.sendRedirect("/buy");
    }

    @Override
    public void init() throws ServletException {
        db = DAOOrderDb.getInstance();
    }
}
