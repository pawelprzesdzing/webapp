package com.sda.przesdzing.servlet.Product;

import com.sda.przesdzing.servlet.DAO.DAOImpl;
import com.sda.przesdzing.servlet.Domain.ProductV2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

@WebServlet(urlPatterns = "/viewDBProduct")
public class ProductDBViewServlet extends HttpServlet {
    private Optional<ProductV2> products;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        String id = httpServletRequest.getParameter("id");
        DAOImpl daoimpl = new DAOImpl();
        try {
            products = daoimpl.getProductById(Integer.parseInt(id));
            httpServletRequest.setAttribute("product", daoimpl.getProductById(Integer.valueOf(id)));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Cookie cookie = new Cookie("lastView", id);
        httpServletResponse.addCookie(cookie);

        httpServletRequest
                .getRequestDispatcher("/viewProduct.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }
}
