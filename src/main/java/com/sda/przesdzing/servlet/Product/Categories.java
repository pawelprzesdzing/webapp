package com.sda.przesdzing.servlet.Product;

public enum Categories {
    LIGHT_TOOLS,
    HEAVY_TOOLS,
    METAL_DEPARTMENT,
    BUILDER_DEPARTMENT,
    PAINTING_DEPARTMENT,
    PROTECTIVE_MATERIALS_DEPARTMENT
}
