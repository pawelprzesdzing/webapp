package com.sda.przesdzing.servlet.Product;

import com.sda.przesdzing.servlet.DAO.DAOImpl;
import com.sda.przesdzing.servlet.Domain.ProductV2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(urlPatterns = "/dbproducts")
public class DbProducts extends HttpServlet {
    private ArrayList<ProductV2> products;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        DAOImpl daoimpl = new DAOImpl();
        try {
            products = daoimpl.getAllProducts();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        httpServletRequest.setAttribute("dbproducts", products);


        httpServletRequest.getRequestDispatcher("/dbproducts.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }
}
